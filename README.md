CakePHP Responsive template for bake shell
==========================================

Screenshots
-----------

* http://i.imgur.com/u3ykJYy.png - Index listing
* http://i.imgur.com/4SZlS9T.png - Single item view
* http://i.imgur.com/YfrPz8K.png - Item edit/create form

Installation
------------

Download the latest source and put the folders either in `ROOT/app/Console/Templates` or `ROOT/lib/Cake/Console/Templates` depending on if you want to use your application console executable (`ROOT/app/Console/cake`) or the library executable (`ROOT/lib/Cake/Console/cake`) and share the responsive templates along different cake projects.

Usage
-----

### First time usage

* `cd /path/to/app`
* `Console/cake bake project` - this is mandatory, as the skeleton ships with `RFormHelper` class which is needed.

### Generate responsive views

Simply use bake shell as usual
`cake bake all Product` will bake Model, Controller and all the CRUD views for your products.

License
-------

Use wherever you find useful, as long as you include the license.