<?php

App::uses('FormHelper', 'View/Helper');


/**
 * Form helper library.
 *
 * Automatic generation of HTML FORMs from given data.
 *
 * @package       Cake.View.Helper
 * @property      HtmlHelper $Html
 * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/form.html
 */
class RFormHelper extends FormHelper {

	public function input($field, $options = array()) {
		$responsiveOptions = array(
			'div' => 'form-group',
			'class' => 'form-control'
		);
		$options = array_merge($responsiveOptions, $options);
		return parent::input($field, $options);
	}

	public function create($model = null, $options = array()) {
		$responsiveOptions = array(
			'role' => 'form'
		);
		$options = array_merge($responsiveOptions, $options);
		return parent::create($model, $options);
	}

	public function end($options = array()) {
		if(!is_array($options)) {
			$options = array(
				// 'label' => $options,
				'div'=>false,
				'class' => 'btn btn-default'
			);
		}
		$responsiveOptions = array('label' => 'Submit', 'div'=>false,'class'=>'btn btn-success');
		$options = array_merge($responsiveOptions, $options);
		return parent::end($options);
		
	}
}
